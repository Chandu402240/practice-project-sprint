package com.hendisantika.eureka.customer.shared.client.controller;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/5/17
 * Time: 9:25 AM
 * To change this template use File | Settings | File Templates.
 */

public class MessageWrapper<T> {

    private T wrapped;
    private String message;

    public MessageWrapper(T wrapped, String message) {
        this.wrapped = wrapped;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public T getWrapped() {
        return wrapped;
    }

}
